package org.JacketCraft.Jacket;

import java.lang.reflect.Array;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;

import org.JacketCraft.Jacket.network.Connection;
import org.JacketCraft.Jacket.network.PacketListenerRunnable;
import org.JacketCraft.Jacket.network.Player;
import org.JacketCraft.Jacket.world.Difficulty;
import org.JacketCraft.Jacket.world.GameMode;

public class Server {
	private static Thread pt;
	private long serverid;
	private boolean invisible;
	private final int port;
	private final String motd;
	private final Difficulty difficulty;
	private final GameMode gamemode;
	private final LinkedList<Player> players = new LinkedList<Player>();
	private static final Integer CURRENT_VERSION = 5;
	private static final String VERSION = "Alpha 0.8.1";
	private static final String JACKETCRAFT_RELEASE = "dev_a0.0.2nw";
	private static final Integer RELEASE_TYPE = 1;

	// For Developers
	//
	// a = alpha
	// b = beta
	// r = release
	// w = working
	// nw = not working
	// x.x.x = Version
	// dev = development
	// 1 = Doesn't work
	// 2 = May work
	// 3 = Does work.

	protected Server(HashMap<String, Object> options) throws Exception {
		difficulty = Difficulty
				.getById((options.containsKey("difficulty") ? Integer
						.valueOf(options.get("difficulty").toString())
						: Integer.valueOf(1)));
		gamemode = GameMode.getById((options.containsKey("gamemode") ? Integer
				.valueOf(options.get("gamemode").toString()) : Integer
				.valueOf(0)));
		port = (options.containsKey("port") ? Integer.valueOf(options.get(
				"port").toString()) : 19132);
		invisible = (options.containsKey("invisible") ? Boolean.valueOf(options
				.get("invisible").toString()) : false);
		motd = (options.containsKey("motd") ? options.get("motd").toString()
				: "Default JacketCraft Server");
		DatagramSocket sock;
		sock = new DatagramSocket(port);
		serverid = new Random().nextLong();
		PacketListenerRunnable run = new PacketListenerRunnable(sock);
		pt = new Thread(run);
		pt.start();
				System.out.println("Starting Minecraft Pocket Edition Server for Version "
						+ VERSION + " using JacketCraft " + JACKETCRAFT_RELEASE);
		System.out.println("Server started successfully!" + " Server Id: "
				+ serverid);
				
		if(RELEASE_TYPE == 1){
			System.out.println("This version won't work!");
		}else if(RELEASE_TYPE == 2){
			System.out.println("This version will most likely work!");
		}else if(RELEASE_TYPE == 3){
			System.out.println("This version works!");
		}else{
			System.out.println("[DEBUG] There's no Version state set. Current: " + RELEASE_TYPE);
		}		
	}

	public long getID() {
		return serverid;
	}

	public boolean isInvisible() {
		return invisible;
	}

	public Player[] getPlayers() {
		return players.toArray((Player[]) Array.newInstance(Player.class,
				players.size()));
	}

	public String getMOTD() {
		return motd;
	}

	public Difficulty getDifficulty() {
		return difficulty;
	}

	public GameMode getDefaultGamemode() {
		return gamemode;
	}

	public int getProtocolVersion() {
		return CURRENT_VERSION;
	}

	public String getVersion() {
		return VERSION;
	}

	public int getPort() {
		return port;
	}

	public void addPlayer(Player p) {
		if (p == null)
			return;
		if (!players.contains(p)) {
			players.add(p);
		}
	}

	public Player getPlayer(String name) {
		for (Player p : players) {
			if (p.getName().equals(name))
				return p;
		}
		return null;
	}

	public Player getPlayer(Connection c) {
		for (Player p : players) {
			if (p.getConnection().equals(c))
				return p;
		}
		return null;
	}

	public Player getPlayer(InetAddress add, int port) {
		for (Player p : players) {
			if (p.getConnection().getAddress().equals(add)
					&& p.getConnection().getPort() == port)
				return p;
		}
		return null;
	}
}
