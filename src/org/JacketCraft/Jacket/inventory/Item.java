package org.JacketCraft.Jacket.inventory;

public abstract class Item{

     private int amount = 0;
   //  private int maxAmount = 64;
     //can be tool or something else
     private String type = "item";
     //Item types:
     //item
     //tool_pickaxe
     //tool_sword
     //tool_shovel
     //tool_axe
     //tool_hoe
     //tool
     //food
     //block
     //block_interact
     private int durability = 0;
     
     //different item variants (colored wool, etc.)
     private short data = 0;

     public Item()
     
     public int getAmount(){
          return amount;
     }

     /*public int getMaxAmount(){
          return maxAmount;
     }*/

     public short getData(){
          return data;
     }

     public String getType(){
          return type;
     }

     public int getDurability(){
          return durability;
     }
     
     public boolean equals(Object other){
    	 if(!(other instanceof Item)) return false;
    	 Item otherI = (Item) other;
    	 return otherI.getAmount() == this.getAmount() && otherI.getDurability() == this.getDurability();
     }
}
