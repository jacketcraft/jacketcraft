package org.JacketCraft.Jacket.inventory;

public enum Type {
	PLACEHOLDER(0, 64);
	
	private int id;
	private int maxStackSize;
	private Type(int id, int maxStackSize) {
		this.id = id;
		this.maxStackSize = maxStackSize;
	}
	
	public int getId()
	{
		return id;
	}
	
	public int getStackSize()
	{
		return maxStackSize;
	}
}
