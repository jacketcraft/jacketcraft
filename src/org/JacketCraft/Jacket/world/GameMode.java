package org.JacketCraft.Jacket.world;

import java.util.HashMap;

public enum GameMode {
	SURVIVAL(0),
	CREATIVE(1);
	
	
	private final int id;
	private static final HashMap<Integer, GameMode> BY_ID = new HashMap<Integer, GameMode>();
	
	private GameMode(int id){
		this.id = id;
	}
	public int getId(){
		return this.id;
	}
	public static GameMode getById(int id){
		return BY_ID.get(Integer.valueOf(id));
	}
	
	static{
		for(GameMode gm : values()){
			BY_ID.put(gm.getId(), gm);
		}
	}
}
