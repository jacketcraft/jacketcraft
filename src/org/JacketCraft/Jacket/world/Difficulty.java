package org.JacketCraft.Jacket.world;

import java.util.HashMap;

public enum Difficulty {
	PEACEFUL(0),
	EASY(1),
	NORMAL(2),
	HARD(3);
	
	private final int id;
	private final static HashMap<Integer, Difficulty> BY_ID = new HashMap<Integer, Difficulty>();
	
	private Difficulty(int id){
		this.id = id;
	}
	public int getId(){
		return this.id;
	}
	public static Difficulty getById(int id){
		return BY_ID.get(Integer.valueOf(id));
	}
	
	static{
		for(Difficulty difficulty : values()){
			BY_ID.put(Integer.valueOf(difficulty.getId()), difficulty);
		}
	}

}
