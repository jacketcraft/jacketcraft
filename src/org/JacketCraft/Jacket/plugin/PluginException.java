package org.JacketCraft.Jacket.plugin;

public class PluginException extends Exception{

	private static final long serialVersionUID = 3016002618793246499L;
	
	public PluginException(Throwable cause){
		super(cause);
	}
	public PluginException(String message, Throwable cause){
		super(message, cause);
	}
	public PluginException(String message){
		super(message);
	}

}
