package org.JacketCraft.Jacket.plugin;

public class JacketPlugin {
	
	private boolean enabled = false;
	
	public JacketPlugin(){
		
	}
	
	public void onEnable(){
		
	}
	public void onDisable(){
		
	}
	public final boolean isEnabled(){
		return enabled;
	}
	public void setEnabled(boolean bool){
		enabled = bool;
		if(enabled)
			onEnable();
		else 
			onDisable();
	}

}
