package org.JacketCraft.Jacket.plugin;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.JacketCraft.Jacket.Server;

public class PluginLoader {
	
	private final Server server;
	
	public PluginLoader(Server server){
		if(server == null){
			throw new IllegalArgumentException("Server cannot be null");
		}
		this.server = server;
	}
	
	public JacketPlugin loadPlugin(File file)throws PluginException{
		if(file == null){
			throw new IllegalArgumentException("File cannot be null");
		}
		if(!file.exists()){
			throw new PluginException(new FileNotFoundException(file.getPath() + " does not exists"));
		}
		
		PluginDescriptionFile descriptionFile = null;
		descriptionFile = getDescriptionFile(file);
		return null;
	}
	
	
	public PluginDescriptionFile getDescriptionFile(File file)throws PluginException{
		if(file == null){
			throw new IllegalArgumentException("File cannot be null");
		}
		JarFile jarFile = null;
		InputStream stream = null;
		JarEntry jarEntry = null;
		try{
			jarFile = new JarFile(file);
			jarEntry = jarFile.getJarEntry("plugin.txt");
			
			if(jarEntry == null){
				throw new PluginException(new FileNotFoundException("Jar doesn't contain a plugin.txt"));
			}
			stream = jarFile.getInputStream(jarEntry);
			return new PluginDescriptionFile(stream);
		}catch(IOException e){
			throw new PluginException(e);
		}finally{
			if(jarFile !=null)
				try{
					jarFile.close();
			}catch(IOException e){
			}
			if(stream !=null)
				try{
					stream.close();
				}catch(IOException e){
				}
		}
	}
}
