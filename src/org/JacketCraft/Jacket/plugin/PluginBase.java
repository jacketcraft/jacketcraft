package org.JacketCraft.Jacket.plugin;

public interface PluginBase {
	public void onEnable();
	public void onDisable();
        public void onLoad();
        public void onError();
}
