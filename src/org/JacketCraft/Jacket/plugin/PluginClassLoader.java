package org.JacketCraft.Jacket.plugin;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

public class PluginClassLoader extends URLClassLoader{
	
	private final PluginLoader pluginLoader;
	private final PluginDescriptionFile descriptionFile;
	private final File folder;
	private final File file;
	private JacketPlugin plugin;
	
	public PluginClassLoader(PluginLoader pluginLoader, ClassLoader classLoader, PluginDescriptionFile descriptionFile, File folder, File file) throws MalformedURLException, PluginException{
		super(new URL[]{file.toURI().toURL()}, classLoader);
		if(pluginLoader == null){
			throw new IllegalArgumentException("PluginLoader cannot be null");
		}
			this.pluginLoader= pluginLoader;
			this.descriptionFile = descriptionFile;
			this.folder = folder;
			this.file = file;
			
			Class jarClass = null;
			Class pluginMain = null;
			try {
				jarClass = Class.forName(descriptionFile.getMain(), true, this);
			} catch (ClassNotFoundException e) {
				throw new PluginException("Cannot find main class: " + descriptionFile.getMain(), e);
			}
			try{
				pluginMain = jarClass.asSubclass(JacketPlugin.class);
			}catch(ClassCastException e){
				throw new PluginException("Main class: " + descriptionFile.getMain() + " doesn't extend JacketPlugin", e);
			}
			try {
				this.plugin = (JacketPlugin)pluginMain.newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
			}

	}
}
