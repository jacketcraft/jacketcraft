package org.JacketCraft.Jacket;

import org.JacketCraft.Jacket.network.Player;
import org.JacketCraft.Jacket.world.Difficulty;


public final class Jacket {
	private static Server server;
	public static int getCurrentProtocolVersion() 
	{
		return server.getProtocolVersion();
	}
	public static Server getServer()
	{
		return server;
	}
	
	public static void setServer(Server server)
	{
		if(Jacket.server != null)
		{
			System.out.println("Server could not be set.");
			return;
		}
		Jacket.server = server;
	}
	
	public static long getServerID()
	{
		return server.getID();
	}
	
	public static String getVersion()
	{
		return server.getVersion();
	}
	public static Player[] getPlayers(){
		return server.getPlayers();
	}
	public static String getMOTD(){
		return server.getMOTD();
	}
	public static boolean isInvisible(){
		return server.isInvisible();
	}
	public static Difficulty getDifficulty(){
		return server.getDifficulty();
	}
}
