package org.JacketCraft.Jacket.utils;

import java.io.DataOutputStream;
import java.io.IOException;

public class PacketUtils {

	public static void writeMagic(DataOutputStream out) throws IOException {
		out.write(0);
		out.write(0xff);
		out.write(0xff);
		out.write(0);
		out.writeInt(0xfefefefe);
		out.writeInt(0xfdfdfdfd);
		out.writeInt(0x12345678);
	}
}
