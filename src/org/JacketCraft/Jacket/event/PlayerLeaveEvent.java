package org.JacketCraft.Jacket.event;

import org.JacketCraft.Jacket.network.Player;

public class PlayerLeaveEvent extends PlayerEvent{
	
	private String message;

	public PlayerLeaveEvent(Player p, String message) {
		super(p);
		setLeaveMessage(message);
	}
	
	public String getLeaveMessage(){
		return message;
	}
	public void setLeaveMessage(String message){
		this.message = message;
	}

}
