package org.JacketCraft.Jacket.event;

import org.JacketCraft.Jacket.network.Player;

public class PlayerJoinEvent extends PlayerEvent{
	
	private String message;

        //Join messages will only be able to work with
        //the console. The IG join Message is automatically
        //sent by the MCPE Client. This is not cancleable.
        //
        //~Niki

	public PlayerJoinEvent(Player p, String message) {
		super(p);
		setJoinMessage(message);
	}
	
	public String getJoinMessage(){
		return message;
	}
	public void setJoinMessage(String message){
		this.message = message;
	}

}
