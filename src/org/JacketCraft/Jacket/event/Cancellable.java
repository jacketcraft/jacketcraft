package org.JacketCraft.Jacket.event;

public interface Cancellable {
	public boolean isCancelled();
	public void setCancelled(boolean cancel);
}
