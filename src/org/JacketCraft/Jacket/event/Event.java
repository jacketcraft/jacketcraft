package org.JacketCraft.Jacket.event;

public abstract class Event {

	private String name;

	public String getEventName() {
		if (this.name == null) {
			this.name = getClass().getSimpleName();
		}
		return this.name;
	}
}
