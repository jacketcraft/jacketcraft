package org.JacketCraft.Jacket.event;

import org.JacketCraft.Jacket.network.Player;

public abstract class PlayerEvent extends Event {
	private Player player;

	public PlayerEvent(Player p) {
		super();
		player = p;
	}

	public Player getPlayer() {
		return player;
	}
}
