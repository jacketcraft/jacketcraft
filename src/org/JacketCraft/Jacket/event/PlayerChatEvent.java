package org.JacketCraft.Jacket.event;

import org.JacketCraft.Jacket.network.Player;

public class PlayerChatEvent extends PlayerEvent implements Cancellable {
	private String message;
	private boolean isCancelled;

	public PlayerChatEvent(Player p, String message) {
		super(p);
		// broadCast(message);
	}

	public String getChatMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public boolean isCancelled() {
		return isCancelled;
	}

	@Override
	public void setCancelled(boolean cancel) {
		isCancelled = cancel;
	}

}
