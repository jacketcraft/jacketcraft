package org.JacketCraft.Jacket.network;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;

public class Connection {
	private final InetAddress add;
	private final int port;
	private DataPacket lastSent;
	public Connection(InetAddress address, int port) {
		add = address;
		this.port = port;
	}

	public void sendDataPacket(DataPacket packet) throws IOException {
		byte[] data = packet.toByteArray();
		PacketListenerRunnable.getServerSocket().send(
				new DatagramPacket(data, data.length, add, port));
		lastSent = packet;
	}

	public InetAddress getAddress() {
		return add;
	}

	public int getPort() {
		return port;
	}

	public DataPacket getLastSentPacket()
	{
		return lastSent;
	}
	
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!(o instanceof Connection))
			return false;
		Connection c = (Connection) o;
		return getAddress().equals(c) && getPort() == port;
	}
}
