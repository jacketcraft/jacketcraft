package org.JacketCraft.Jacket.network;

import static org.JacketCraft.Jacket.network.PacketType.MC_CLIENT_CONNECT;
import static org.JacketCraft.Jacket.network.PacketType.MC_CLIENT_HANDSHAKE;
import static org.JacketCraft.Jacket.network.PacketType.MC_LOGIN;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.Random;

import org.JacketCraft.Jacket.Jacket;
import org.JacketCraft.Jacket.inventory.PlayerInventory;
import org.JacketCraft.Jacket.world.GameMode;

public final class Player {
	private boolean generating;
	private int protocol1;
	private int protocol2;
	private GameMode gamemode;
	private final Connection conn;
	private String name;
	private final byte[] counter = new byte[] { 0, 0, 0 };
	private long clientid;
	private final int MTU;

	protected Player(InetAddress add, int port, long clientid, int mtusize) {
		conn = new Connection(add, port);
		this.clientid = clientid;
		MTU = mtusize;
		gamemode = Jacket.getServer().getDefaultGamemode();
	}

	public void sendDataPacket(byte[] payload) throws IOException {
		counter[0]++;
		DataPacket packet = new DataPacket(0x80, counter, payload);
		getConnection().sendDataPacket(packet);
	}

	public Connection getConnection() {
		return conn;
	}

	public void setGameMode(GameMode gm) {
		if (!gamemode.equals(gm)) {
			kick();
			// TODO
		}
	}

	public GameMode getGameMode() {
		return gamemode;
	}

	public boolean kick() {
		// TODO
                return true;
	}
	
	public boolean ban(){
		//TODO
                return true;
	}
	
	public long getCID(){
		return clientid;
	}
	
	public boolean isOnline(){
		return false;
		//Just for improvisation
	}
	
	/*public void addHealth(Double Health){

		//TODO: Add health
                
	}
	
	public void looseHealth(Double Health){

		//TODO: Add health

	}*/
	public void setHealth(Double Health){
		
		//TODO: Add health

	}
	
	public void ChatAsPlayer(String Message){
		//TODO
	}
	
	public boolean isFlying(){
		return false;
		//TODO
	}
	
	public PlayerInventory getInventory(){
		return null;
		//TODO
	}
	
	/*public void getPets(){
		return false;
		//TODO
	}*/

	public String getName() {
		return name;
	}

	public void sendMessage(String message) {
		// TODO
	}

	public void handleDataPacket(DataPacket packet) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream streamout = new DataOutputStream(baos);
		DataInputStream streamin = new DataInputStream(
				new ByteArrayInputStream(packet.getPayload()));
		System.arraycopy(packet.getCounter(), 0, counter, 0,
				packet.getCounter().length);
		int packetid = streamin.readByte() & 0xff;
		String hexpack = Integer.toHexString(packetid);
		if (hexpack.length() == 1) {
			hexpack = "0x0" + hexpack;
		} else
			hexpack = "0x" + hexpack;
		System.out.println("Datapacket: " + hexpack);
		switch (packetid) {
		case MC_CLIENT_CONNECT:
			streamout.write(0x10);
			streamout.write(0x043f57fe);
			streamout.write(0xcd);
			streamout.writeShort(conn.getPort());
			streamout.write(0xf5fffff5);
			for (int i = 0; i < 9; i++) {
				streamout.write(0xffffffff);
			}
			streamout.write(0x0000);
			long clientid = streamin.readLong();
			streamout.writeLong(this.clientid);
			long sessionid = streamin.readLong();
			streamout.writeLong(sessionid);
			sendDataPacket(baos.toByteArray());
			break;
		case MC_CLIENT_HANDSHAKE:
			break;

		case MC_LOGIN:
			if (!generating) {
				name = streamin.readUTF();
				protocol1 = streamin.readInt();
				protocol2 = streamin.readInt();
				clientid = streamin.readInt();
				String realmsdata = streamin.readUTF();
				streamout.write(0x83);
				streamout.writeInt(0);
				sendDataPacket(baos.toByteArray());
				baos = new ByteArrayOutputStream();
				streamout = new DataOutputStream(baos);
				streamout.write(0x87);
				streamout.writeInt(10000000);
				streamout.writeInt(0);
				streamout.writeInt(gamemode.getId());
				streamout.writeInt(0);
				streamout.writeFloat(0);
				streamout.writeFloat(0);
				streamout.writeFloat(0);
				sendDataPacket(baos.toByteArray());
				generating = true;
			}
			break;
		case 0x95:
			streamout.write(0xb5);
			streamout.writeUTF("Hallo, dies ist ein Test! :)");
			sendDataPacket(baos.toByteArray());
		default:
			break;

		}
		// sendPacket(get0xC0());

	}

	public byte[] get0xC0() throws IOException {
		ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
		DataOutputStream outputstream = new DataOutputStream(bytestream);
		outputstream.write(0xC0);
		outputstream.writeShort(1);
		outputstream.writeBoolean(true);
		outputstream.write(counter);
		return bytestream.toByteArray();
	}

	public void sendPacket(byte[] packet) throws IOException {
		PacketListenerRunnable.getServerSocket().send(
				new DatagramPacket(packet, packet.length, conn.getAddress(),
						conn.getPort()));
	}
}
