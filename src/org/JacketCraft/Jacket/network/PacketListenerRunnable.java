package org.JacketCraft.Jacket.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class PacketListenerRunnable implements Runnable {
	private static DatagramSocket socket;

	public PacketListenerRunnable(DatagramSocket sock) {
		socket = sock;
	}

	public void run() {
		while (true) {
			DatagramPacket packet = new DatagramPacket(new byte[2000], 2000);
			try {
				socket.receive(packet);
			} catch (IOException e) {
			}
			try {
				PacketHandler.handle(packet);
			} catch (IOException e) {
				e.printStackTrace();
				System.err.println("Error handling Packet "
						+ (packet.getData()[0] & 0xff));
			}
		}
	}

	protected static DatagramSocket getServerSocket() {
		return socket;
	}

}
