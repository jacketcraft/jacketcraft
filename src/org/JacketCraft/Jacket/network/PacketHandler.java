package org.JacketCraft.Jacket.network;

import static org.JacketCraft.Jacket.network.PacketType.ACK;
import static org.JacketCraft.Jacket.network.PacketType.ID_ADVERTISE_SYSTEM_1;
import static org.JacketCraft.Jacket.network.PacketType.ID_CONNECTED_PING_OPEN_CONNECTIONS;
import static org.JacketCraft.Jacket.network.PacketType.ID_INCOMPATIBLE_PROTOCOL_VERSION;
import static org.JacketCraft.Jacket.network.PacketType.ID_OPEN_CONNECTION_REPLY_1;
import static org.JacketCraft.Jacket.network.PacketType.ID_OPEN_CONNECTION_REPLY_2;
import static org.JacketCraft.Jacket.network.PacketType.ID_OPEN_CONNECTION_REQUEST_1;
import static org.JacketCraft.Jacket.network.PacketType.ID_OPEN_CONNECTION_REQUEST_2;
import static org.JacketCraft.Jacket.network.PacketType.ID_UNCONNECTED_PING_OPEN_CONNECTIONS;
import static org.JacketCraft.Jacket.network.PacketType.NACK;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.Arrays;

import org.JacketCraft.Jacket.Jacket;
import org.JacketCraft.Jacket.utils.PacketUtils;

public class PacketHandler {
	private static byte[] current;
	protected static InetAddress currentAdd;
	protected static int currentPort;
	protected static int currentSignedIntType;
	private static byte[] crntcnt;

	protected static void handle(DatagramPacket received) throws IOException {
		current = getReal(received.getData(), received.getLength());
		currentAdd = received.getAddress();
		currentPort = received.getPort();
		currentSignedIntType = current[0] & 0xff;
		// System.out.println(currentSignedIntType);
		switch (currentSignedIntType) {
		case ID_CONNECTED_PING_OPEN_CONNECTIONS:
		case ID_UNCONNECTED_PING_OPEN_CONNECTIONS:
			byte[] resp0x1c = response0x1c();
			PacketListenerRunnable.getServerSocket().send(
					new DatagramPacket(resp0x1c, resp0x1c.length, received
							.getAddress(), received.getPort()));
			break;
		case ID_OPEN_CONNECTION_REQUEST_1:
			byte[] resp0x06or0x1A = response0x06or0x1A();
			PacketListenerRunnable.getServerSocket().send(
					new DatagramPacket(resp0x06or0x1A, resp0x06or0x1A.length,
							received.getAddress(), received.getPort()));
			break;
		case ID_OPEN_CONNECTION_REQUEST_2:
			respondWith0x08();
			break;
		case 0x80:
		case 0x81:
		case 0x82:
		case 0x83:
		case 0x84:
		case 0x85:
		case 0x86:
		case 0x87:
		case 0x88:
		case 0x89:
		case 0x8a:
		case 0x8b:
		case 0x8c:
		case 0x8d:
		case 0x8e:
		case 0x8f:
			handleDataPacket();
			/*
			 * byte[] res0xC0 = get0xC0();
			 * PacketListenerRunnable.getServerSocket().send( new
			 * DatagramPacket(res0xC0, res0xC0.length, received .getAddress(),
			 * received.getPort()));
			 */
			break;
		// read0xC0();
		/*
		 * byte[] resp0xC0 = get0xC0();
		 * PacketListenerRunnable.getServerSocket().send( new
		 * DatagramPacket(resp0xC0, resp0xC0.length, received .getAddress(),
		 * received.getPort()));
		 */
		default:
			System.out.println(currentSignedIntType + "!");
			break;
		}
		current = null;
		currentAdd = null;
		currentPort = -1;
	}

	private static byte[] getReal(byte[] ba, int length) {
		int realSize = length;
		byte[] realPacket = new byte[realSize];
		System.arraycopy(ba, 0, realPacket, 0, realSize);
		return realPacket;
	}

	/*
	 * private static byte[] response0xC0() throws IOException {
	 * ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
	 * DataOutputStream outputstream = new DataOutputStream(bytestream);
	 * DataInputStream inputstream = cIn(); inputstream.skipBytes(1); short
	 * unknown = inputstream.readShort(); boolean anotherpacket =
	 * inputstream.readBoolean(); byte[] ba = new byte[3]; inputstream.read(ba);
	 * System.out.println(Arrays.toString(ba)); System.out.println(unknown + ":"
	 * + anotherpacket); outputstream.write(ba); return
	 * bytestream.toByteArray(); }
	 */

	protected static void read0xC0() throws IOException {
		ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
		DataOutputStream outputstream = new DataOutputStream(bytestream);
		DataInputStream inputstream = cIn();
		inputstream.skipBytes(1);
		System.out.println(inputstream.readShort());
		System.out.println(inputstream.readBoolean());
		byte[] b = new byte[3];
		inputstream.read(b);
	}

	private static void handleDataPacket() throws IOException {
		// outputPacketData(current);
		System.out.println("Received from: " + currentAdd.toString() + " : " + currentPort);
		DataInputStream inputstream = cIn();
		int datapacketid = inputstream.readByte() & 0xff;
		System.out.println("Datapacket: " + datapacketid);
		byte[] count = new byte[3];
		inputstream.read(count);
		crntcnt = count;
		int encaps = inputstream.readByte();
		System.out.println("Encaps: " + encaps);
		short length = (short) (inputstream.readShort() / 8);
		System.out.println("Length: " + length);
		if (encaps == 0x40 || encaps == 0x60) {
			byte[] cnt = new byte[3];
			inputstream.read(cnt);
			if (encaps == 0x60) {
				byte[] unknown = new byte[4];
				inputstream.read(unknown);
			}
		}
		byte[] payload = new byte[length];
		inputstream.read(payload);
		if (encaps == 0x00)
			System.out.println(Arrays.toString(payload));
		Jacket.getServer()
				.getPlayer(currentAdd, currentPort)
				.handleDataPacket(
						new DataPacket(datapacketid, crntcnt, payload));
		return;
		/*
		 * if (nextpacket == 0x00) { int length = inputstream.readByte();
		 * System.out.println("length" + length);
		 * System.out.println(inputstream.readByte()); }
		 */
		/*
		 * outputstream.write(0x80); crntcnt[0]++; outputstream.write(crntcnt);
		 * outputstream.write(0x00);
		 * 
		 * return bytestream.toByteArray();
		 */
	}

	private static byte[] get0xC0() throws IOException {
		ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
		DataOutputStream outputstream = new DataOutputStream(bytestream);
		DataInputStream inputstream = cIn();
		outputstream.write(0xC0);
		outputstream.writeShort(1);
		outputstream.writeBoolean(true);
		outputstream.write(0x00);
		return bytestream.toByteArray();
	}

	private static byte[] response0x1c() throws IOException {
		ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
		DataOutputStream outputstream = new DataOutputStream(bytestream);
		DataInputStream inputstream = cIn();
		inputstream.skipBytes(1);
		long pingid = inputstream.readLong();
		outputstream.write(ID_ADVERTISE_SYSTEM_1);
		outputstream.writeLong(pingid);
		outputstream.writeLong(Jacket.getServerID());
		PacketUtils.writeMagic(outputstream);
		outputstream.writeUTF("MCCPP;Demo;"
				+ (!Jacket.getServer().isInvisible() ? Jacket.getServer()
						.getMOTD() : ""));
		return bytestream.toByteArray();
	}

	private static byte[] response0x06or0x1A() throws IOException {
		ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
		DataOutputStream outputstream = new DataOutputStream(bytestream);
		DataInputStream inputstream = cIn();
		int mtusize = current.length - 18;
		inputstream.skipBytes(17);
		byte version = inputstream.readByte();
		if (version != Jacket.getCurrentProtocolVersion()) {
			outputstream.write(ID_INCOMPATIBLE_PROTOCOL_VERSION);
			PacketUtils.writeMagic(outputstream);
			outputstream.writeLong(Jacket.getServerID());
		} else {
			outputstream.write(ID_OPEN_CONNECTION_REPLY_1);
			PacketUtils.writeMagic(outputstream);
			outputstream.writeLong(Jacket.getServerID());
			outputstream.write(0x00);
			outputstream.writeShort(mtusize);
		}
		return bytestream.toByteArray();
	}

	private static void respondWith0x08() throws IOException {
		ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
		DataOutputStream outputstream = new DataOutputStream(bytestream);
		DataInputStream inputstream = cIn();
		inputstream.skipBytes(22);
		short udpport = inputstream.readShort();
		short mtusize = inputstream.readShort();
		long clientid = inputstream.readLong();
		outputstream.write(ID_OPEN_CONNECTION_REPLY_2);
		PacketUtils.writeMagic(outputstream);
		outputstream.writeLong(Jacket.getServerID());
		outputstream.writeShort(currentPort);
		outputstream.writeShort(mtusize);
		outputstream.write(0x00);
		byte[] resp0x08 = bytestream.toByteArray();
		PacketListenerRunnable.getServerSocket().send(
				new DatagramPacket(resp0x08, resp0x08.length, currentAdd,
						currentPort));
		System.out.println("sent");
		Jacket.getServer().addPlayer(
				new Player(currentAdd, currentPort, clientid, mtusize));
	}

	private static void outputPacketData(byte[] data) {
		for (int i = 0; i < data.length; i++) {
			byte data1 = data[i];
			if (data1 != 0)
				System.out.println("Location: " + i + ", Data: " + data1);
		}
	}

	private static DataInputStream cIn() {
		return new DataInputStream(new ByteArrayInputStream(current));
	}
}
