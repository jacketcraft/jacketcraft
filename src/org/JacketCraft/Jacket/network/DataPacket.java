package org.JacketCraft.Jacket.network;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class DataPacket {
	private final int packetId;
	private final byte[] counter;
	private final byte[] payload;

	public DataPacket(int datapacketid, byte[] count, byte[] payload) {
		packetId = datapacketid;
		counter = count;
		System.out.println(payload[0] & 0xff);
		this.payload = payload;
	}

	public int getDataPacketId() {
		return packetId;
	}

	public byte[] getCounter() {
		return counter;
	}

	public byte[] getPayload() {
		return payload;
	}

	public byte[] toByteArray() throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream outputstream = new DataOutputStream(baos);
		outputstream.write(0x80);
		outputstream.write(counter);
		outputstream.write(0x00);
		outputstream.writeShort(payload.length * 8);
		outputstream.write(payload);
		return baos.toByteArray();
	}
}
